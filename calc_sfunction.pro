PRO calc_gen_cor_dim
 ; This routine calculates the structure function inertial range index for a range of q values.
 ; It calls sf_spectrum to produce the values
 
  dir1 ='/code/'

  ; IMPORT PARAMETERS
  ini_params, dir1+'params.json', arr=params
  params = JSON_PARSE(strjoin(params),/TOSTRUCT)
  
  ; READ IN SOME GLOBAL PARAMETERS
  ini_date = urldate(params.algorithm.start_time)   ; INITIAL DATE
  fin_date = urldate(params.algorithm.end_time)     ; FINAL DATE
  provenance = params.algorithm.lon_max             ; PROVENANCE NAME FOR WRITING INTO DB
  comp = params.algorithm.comp               ;
  ; PARAMS FOR SPECIFIC PROPERTY EXTRACTION
  q = params.algorithm.q                            ; selectors array for which inertial range index will be calculated 
  bz_th = params.algorithm.bz_th                    ; minimum magnetic field strength to be considered
  comp = params.algorithm.comp                      ; component to be used (default Br, but it may be also Blos, Btot, Bp, Bt, Bhor)
  
  ; CREATING A JSON STRUCTURE FOR REPORTING RESULTS
  pe_result = {algorithm_name: 'sf_spectrum',algorithm_parameters: {zq: 0.0, err_zq: 0.0, selectors: q, Bz_Thress: bz_th, component: comp } }
  post_data = {long_hg: 0., time_start: '', lat_hg: 0., long_carr: 0., nar: 0, data: pe_result}

; OBTAIN FILES URLS AND METADATA FROM SERVER USING API SERVER. RESULT IS A STRUCTURE (DATA_STR) WITH FILES AT THE
  ; SET CADENCE. /MIDNIGHT CORRESPOND TO ONE A DAY AT TIME 00:00. NONE FOR ALL THE FILES BETWEEN THE SELECTED DATES
  retrieve_data, ini_date, fin_date, data_str, sz, 'none'

  ; LOOP OVER ALL TIMES+SHARP REGIONS IN RANGE
  for i=0, sz(0)-1 do begin
  
      get_files, data_str[i], comp, files_out, f_meta, t_obs
    
      ; OPENING FITS FILES
      read_sdo, files_out[0], ind, unc_img, /silent
      
    ; CORRECTING IMAGES FOR ANY ARTIFACTS (E.G. LIMB PRESENCE)
    image_check_correct, unc_img, ind, f_meta, c_img, pos_out, new_ind
    
    ; CALCULATING B EFFECTIVE
    thres_img,c_img,thres=thres,img_thresed
    SF_SPECTRUM,im,q,rd,Sf,zq,err_zq,/xpos,/xneg,/ypos,/yneg,/default_rd,/auto
    post_data.data.algorithm_parameters.zq = zq
    post_data.data.algorithm_parameters.err_zq = err_zq
    pe_result.algorithm_parameters.zq = zq
    pe_result.algorithm_parameters.err_zq = err_zq
    ; INSERTING THE AR INFORMATION INTO THE JSON STR
    make_json4db, pe_result, pos_out[0], pos_out[1], t_obs, pos_out[2], fix(f_meta.noaa_ars), json_data

    ; SEND THE PROPERTY TO THE DATA BASE
    push_data2db, provenance, json_data

    ; DELETING FILES
    spawn, 'rm -f '+files_out[0]
    
    ; stop
  endfor
  ;
END