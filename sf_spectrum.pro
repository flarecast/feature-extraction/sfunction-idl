PRO SF_SPECTRUM,im,q,rd,Sf,zq,err_zq,bz_thres=bz_thres,xpos=xpos,ypos=ypos,xneg=xneg,$
yneg=yneg,default_rd=default_rd,default_q=default_q,manual_index=manual_index,$
auto_index=auto_index,help=help

; PURPOSE:
; Calculation of the multifractal structure function spectrum on a 2D
; image as described by Abramenko et al. (2002), (2003)
; Programmer: Manolis K. Georgoulis (JHU/APL, 01/25/05)

; MODIFICATION HISTORY:
; * Added the keywords /manual_index and /auto_index for an
;   interactive and automatic detection, respectively, of the
;   power-law regime -- MKG, 12/12/05

if keyword_set(help) then begin
print,'  '
print,'SYNTAX:'
print,'sf_spectrum,im,q,rd,Sf,zq,err_zq,/xpos,/ypos,/xneg,/yneg,/default_q,default_rd,/help'
print,'  '
print,'INPUT:'
print,'im --> Two dimensional input image (Os required in locations of zero information)'
print,'q --> The array of selectors (normally positive real numbers excluding 0)'
print,'rd --> The array of displacement magnitudes, in units of pixel size'
print,'       (a sufficient dynamical range of two or more orders of magnitude is required)'
print,'  '
print,'OUTPUT:'
print,'Sf --> Array [n(q),n(rd)] where n(q), n(rd) are the number of elements'
print,'       of q and rd arrays respectively. It provides the structure function'
print,'       value for each q and rd'
print,'zq --> Array [n(q)] of the structure function exponents for each q'
print,'       provided by power-law fitting between Sf(q,*) and rd(*)'
print,'err_zq --> Uncertainties associated with each zq value'
print,'  '
print,'KEYWORDS:'
print,'/xpos --> The displacement vector is oriented toward positive x-axis values'
print,'/xneg --> The displacement vector is oriented toward negative x-axis values'
print,'/ypos --> The displacement vector is oriented toward positive y-axis values'
print,'/yneg --> The displacement vector is oriented toward negative y-axis values'
print,'/default_q --> Set to use the default values for q as follows:'
print,'         * q in (0.5,8) with step 0.5'
print,'/default_rd --> Set to use the default values for rd as follows:'
print,'         * rd in (1,300) pixels with step=1 between (1,10)'
print,'                                     step=5 between (15,100)'
print,'                                     step=10 between (110,300)'      
print,'/manual_index --> If set, the exponents zq and their errors err_zq are found'
print,'                  interactively, by manually choosing the extrema of the'
print,'                  power-law regime'
print,'/auto_index --> If set, the exponents zq and their errors err_zq are found'
print,'                automatically, via a detection of the power-law regime'
print,'/help --> Use alone to obtain the above ignformation'
print,'  '
return
endif
; MODIFICATIONS HISTORY:
; - Imposed maximum displacement, set to 50% of the mininum linear
;   size of the image (MKG, 09/06/07)
; - Separated default rd- from default q- values via DEFAULT_Q and
;   DEFAULT_RD keywords (MKG, 09/06/07)
; - Added bz_thres keyword set to zero non-interesting areas.

; check image size

if keyword_set(bz_thres) then bzthres=bz_thres else bzthres=100.

; flux thresholding:
slo=where(abs(im) lt bzthres,nlo)
if nlo gt 0 then im(slo)=0.

sz=size(im)
if sz(1) le sz(2) then min_sz=sz(1) else min_sz=sz(2)

if keyword_set(default_q) then  q=findgen(16)/2.+0.5

if keyword_set(default_rd) then begin 
  r1=findgen(10)+1.
  r2=findgen(18)*5.+15.
  r3=findgen(20)*10.+110.
  rd=fltarr(48) & rd(0:9)=r1(*) & rd(10:27)=r2(*) & rd(28:47)=r3(*)
endif
; Impose maximum displacement equal to 50% of the minimum linear size
r1=where(rd le 0.5*min_sz) & rd=rd(r1)

res=size(im) & id1=res(1) & id2=res(2) 
rd_max=max(rd)
mindim=min([id1,id2])
if rd_max ge mindim then begin 
  r1=where(rd le mindim-1,ic)
  rd=rd(r1)
  rd_max=max(rd)
endif
if keyword_set(xpos) or keyword_set(xneg) then begin 
  nel=long(float(id1-rd_max)*float(id2)) 
  j0=0 & j1=id2-1
  if keyword_set(xpos) then begin & i0=0 & i1=id1-1-rd_max &endif
  if keyword_set(xneg) then begin & i0=rd_max & i1=id1-1 &endif
endif
if keyword_set(ypos) or keyword_set(yneg) then begin
  nel=long(float(id1)*float(id2-rd_max))
  i0=0 & i1=id1-1
  if keyword_set(ypos) then begin & j0=0 & j1=id2-1-rd_max & endif
  if keyword_set(yneg) then begin & j0=rd_max & j1=id2-1 &endif
endif
ref=lonarr(nel)
ic=-1.
for i=i0,i1 do for j=j0,j1 do if im(i,j) ne 0. then begin 
ic=ic+1. & ref(ic)=long(float(j)*id1 + float(i)) &endif

tmp=dblarr(nel)

nel_q=n_elements(q)
nel_rd=n_elements(rd)
Sf=dblarr(nel_q,nel_rd)
for m=0,nel_rd-1 do begin 
for n=0,nel_q -1 do begin 
if keyword_set(xpos) then tmp=abs(im(ref+rd(m))-im(ref))^q(n)
if keyword_set(xneg) then tmp=abs(im(ref-rd(m))-im(ref))^q(n) 
if keyword_set(ypos) then tmp=abs(im(ref+rd(m)*id1)-im(ref))^q(n)
if keyword_set(yneg) then tmp=abs(im(ref-rd(m)*id1)-im(ref))^q(n)
Sf(n,m)=mean(tmp)
endfor
endfor

zq=dblarr(nel_q)
err_zq=dblarr(nel_q)
if keyword_set(manual_index) then begin 
  res=minmax(Sf) 
  Sf_min=res(0) & Sf_max=res(1)
  plot_oo,rd,Sf(0,*),yst=1,yr=[0.8*Sf_min,1.2*Sf_max],$
  xtitle='Displacement r (pixels)',ytitle='S!Iq!N(r)',chars=1.8,xthick=2.,$
  ythick=2.,thick=2,xst=1,xr=[0.3,300.]
  for i=1,nel_q-1,2 do oplot,rd,Sf(i,*),thick=2
  print,'  '
  print,'Select inertial range limits for rd:'
  print,'Select lower limit:'
  cursor,x1,y1,/data & wait,0.1
  print,'Select upper limit:'
  cursor,x2,y2,/data & wait,0.1

  for i=0,nel_q -1 do begin 
  r1=where(rd ge x1 and rd le x2) & tmp1=rd(r1) & tmp2=Sf(i,r1)
  res=ladfit(alog10(tmp1),alog10(tmp2),absdev=err_tmp1,/double)
  zq(i)=res(1) & err_zq(i)=err_tmp1
  endfor
endif
if keyword_set(auto_index) then begin 
  for i=0,nel_q-1 do begin 
  Sfc=reform(Sf(i,*))
  FIND_PL_REGIME,rd,Sfc,err_tol=0.05,index=pl_index,err_index=err_index
  zq(i)=pl_index
  err_zq(i)=err_index
  endfor
endif

END







